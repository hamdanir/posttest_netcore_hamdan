﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using preTest.Models;
using preTest.Repository;
using System.Data;
using System.Web.Http.ModelBinding;

namespace preTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public PositionController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("PositionList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getPosition()
        {
            var result = _authentication.getPositionList<ModelPosition>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelPosition position)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", position.Name, DbType.String);
            dp_param.Add("createdby", position.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_CreatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = position });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelPosition position, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", position.Name, DbType.String);
            dp_param.Add("createdby", position.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_UpdatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = position });
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_DeletePosition", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
