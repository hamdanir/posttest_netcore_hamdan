﻿using System.ComponentModel.DataAnnotations;
namespace preTest.Models
{
    public class ModelUser
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDPosition { get; set; }

        public string? CompanyName { get; }
        public string? PositionName { get; }
        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        [Required]
        public string? Email { get; set; }

        public string? Telephone { get; set; }

        [Required]
        public string? Username { get; set; }

        [Required]
        public string? Password { get; set; }

        [Required]
        public string? Role { get; set; }

        public int? Flag { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
