﻿using System.ComponentModel.DataAnnotations;

namespace preTest.Models
{
    public class ModelDocument
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDCategory { get; set; }
        public string? CompanyName { get;  }
        public string? CategoryName { get;  }

        [Required]
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int? Flag { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
